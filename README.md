# Networks lecture for MPhil in Computational Biology

Lecture notes for 3rd (of 4) lectures on networks in [computational
biology](https://www.graduate.study.cam.ac.uk/courses/directory/maammpcbi).

![Creative Commons Licence](https://i.creativecommons.org/l/by/4.0/88x31.png "license") This work by John Reid is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).
