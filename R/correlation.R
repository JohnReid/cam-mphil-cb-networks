#!/usr/bin/env Rscript

#
# Load packages and configuration
#
source('shared.R')
width <- 6.4
height <- 4


#
# Function for plotting
#
corplot <- function(.data) {
  pearsons_r = with(.data, cor(x, y, method = 'pearson'))
  spearmans_rho = with(.data, cor(x, y, method = 'spearman'))
  kendalls_tau = with(.data, cor(x, y, method = 'kendall'))
  plottitle <-
    sprintf(
      "Pearson's r: %.2f; Spearman's rho: %.2f; Kendall's tau: %.2f",
      pearsons_r, spearmans_rho, kendalls_tau)
  ggplot(.data, aes(x=x, y=y)) +
    stat_smooth(method = 'lm', n = 300, se = FALSE) +
    geom_point(size = 5) +
    labs(title = plottitle)
}


#
# Uncorrelated points
#
N <- 77
f <- function(x) .8 * x + .3
noise <- .4
.data <-
  data.frame(x = runif(N, -1, 1), y = runif(N, -1, 1))
gp <- corplot(.data)
print(gp)
ggsave('../Figures/correlation-unc.png', width=width, height=height, units='in', gp)


#
# Linear points
#
N <- 77
f <- function(x) .8 * x + .3
noise <- .4
.data <-
  data.frame(x = runif(N, -1, 1)) %>%
  mutate(y_noiseless = f(x), y = y_noiseless + rnorm(n=N, sd=noise))
gp <- corplot(.data)
print(gp)
ggsave('../Figures/correlation-lin.png', width=width, height=height, units='in', gp)


#
# Sigmoid
#
N <- 77
f <- function(x) 1 / (1 + exp(x*30))
noise <- .1
.data <-
  data.frame(x = runif(N, -1, 1)) %>%
  mutate(y_noiseless = f(x), y = y_noiseless + rnorm(n=N, sd=noise))
gp <- corplot(.data)
print(gp)
ggsave('../Figures/correlation-sig.png', width=width, height=height, units='in', gp)


#
# Exponential
#
N <- 77
f <- function(x) exp(abs(4*x))
noise <- 1
.data <-
  data.frame(x = runif(N, -1, 1)) %>%
  mutate(y_noiseless = f(x), y = y_noiseless + rnorm(n=N, sd=noise))
gp <- corplot(.data)
print(gp)
ggsave('../Figures/correlation-exp.png', width=width, height=height, units='in', gp)
