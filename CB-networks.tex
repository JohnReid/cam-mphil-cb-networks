\documentclass{beamer}
\usetheme{metropolis}           % Use metropolis theme
% \useoutertheme[subsection=false]{miniframes}
% \addtobeamertemplate{frametitle}{}{\vspace{-2em}} % decrease
% \setbeameroption{show notes}  % un-comment to see the notes


%
% Some packages
\usepackage{silence}            % To silence some warning
\WarningFilter{biblatex}{Patching footnotes failed}
\usepackage{amsmath}            % math
%\usepackage{anyfontepackage{amssymb} % http://ctan.org/pkg/amssymb
\usepackage{pifont}             % http://ctan.org/pkg/pifont
\newcommand{\cmark}{\ding{51}}
\newcommand{\xmark}{\ding{55}}


%
% PDF metadata
\usepackage{hyperref}
\hypersetup{%
  pdfauthor={John Reid},
  pdftitle={Networks},
  pdfkeywords={network inference},
  pdfcreator={LuaLaTeX},
  pdfproducer={LuaLaTeX},
}


%
% TikZ
\usepackage{tikz}
\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}
\usetikzlibrary{arrows, shapes, decorations.pathmorphing, backgrounds}
\usetikzlibrary{positioning, fit,graphs, graphs.standard, shadows}
\usetikzlibrary{graphs, graphdrawing, calc}
\usepgflibrary{shapes}
\usegdlibrary{trees, layered, force}
\tikzset{
  myshadow/.style={  % custom shadow with tikz
    opacity=.85,
    shadow xshift=0.15,
    shadow yshift=-0.15,
    shade,
    shading=axis,
    shading angle=230},
  >=stealth',
  %Define style for boxes
  punkt/.style={
          rectangle,
          rounded corners,
          draw=black, very thick,
          text width=7.5em,
          minimum height=2em,
          text centered},
  %Define style for images
  image/.style={
          text width=7.5em,
          minimum height=2em,
          text centered},
  % Define arrow style
  pil/.style={
          ->,
          thick,
          shorten <=2pt,
          shorten >=2pt,}
}


%
% Set up biblatex
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  %natbib,
  %bibstyle=numeric,
  %citestyle=reading,
  backend=biber]{biblatex}
%
% We specify the database.
\addbibresource{2018-02-MPhil-CB-networks.bib}
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}
\addtobeamertemplate{footnote}{\vspace{-6pt}\advance\hsize-0.5cm}{\vspace{6pt}}

% To move footnote citations away from beamer chuff at bottom of page
\makeatletter
% Alternative A: footnote rule
\renewcommand*{\footnoterule}{\kern -3pt \hrule \@width 2in \kern 8.6pt}
% Alternative B: no footnote rule
% \renewcommand*{\footnoterule}{\kern 6pt}
\makeatother


%
% Document metadata
\title{Networks (lecture III)}
\subtitle{MPhil in Computational Biology}
\date{February 19, 2018}
\author{John Reid}
\institute{University of Cambridge}

%
% Commands
\newcommand{\diff}{\mathop{}\!\textnormal{d}}
\newcommand{\footlineextra}[1]{
  \begin{tikzpicture}[remember picture,overlay]
      \node[yshift=0ex, xshift=-2ex,anchor=south west] at (current page.south west) {\usebeamerfont{author in head/foot}\hspace{2ex}#1};
  \end{tikzpicture}
}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
% From https://tex.stackexchange.com/a/30723/39498
\makeatletter
\def\blfootnote{\gdef\@thefnmark{}\@footnotetext}
\makeatother

%
% Document
\begin{document}
\maketitle

\section{Network inference}

\begin{frame}{Ill-posed network inference}
  \note{
    \begin{itemize}
      \item Many networks could explain data
      \item Multimodal posterior
    \end{itemize}
  }
  \begin{columns}[c]
    \begin{column}{.55\textwidth}
      \centering
      \begin{equation*}
        R = (V, E) \qquad |V|=G=8 \textnormal{ genes}
      \end{equation*}
      \begin{tikzpicture}
        \graph { subgraph K_n [n=8, clockwise, radius=.4\textwidth, edges={draw opacity=.5}] };
      \end{tikzpicture}
      $|E| = \frac{G(G-1)}{2} = 28$ possible edges \\
      $2^{28} \approx 2.6 \times 10^8$ possible networks
    \end{column}
    \begin{column}{.45\textwidth}
      \centering
      $C \times G$ expression matrix
      \includegraphics[width=\textwidth]{Figures/mESC-expr}
      \begin{itemize}
        \item Test edges independently
        \item Regress genes independently
        \item Module based approaches %: biclustering
        \item Regularisation
        \item Bayesian methods
      \end{itemize}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Regularisation}
  \note{
    TODO: Show generalisation: extend x-axis
    \begin{itemize}
      \item Describe analogy between function fitting and network inference.
      \item Space of polynomials <--> space of networks
      \item Degree of polynomial <--> permissible complexity of network
      \item Occam's razor prefers simplest explanation
      \item Generalisation ability normally better with simpler models
    \end{itemize}
  }
  \includegraphics[width=\textwidth]{Figures/regularisation}
  \begin{itemize}
    \item Reduce search space of possible networks
    \item Favour networks with \emph{few} edges/penalise \emph{complex} networks
  \end{itemize}
\end{frame}


\begin{frame}{Bayesian methods}
  \note{
    \begin{itemize}
      \item Uncertainty important, imagine getting wrong network for drug discovery
      \item Bayesian methods most theoretically appealing but not most successful
    \end{itemize}
  }
  Bayesian inference for a regulatory network $R$ given data $X$:
  \begin{equation*}
    \underbrace{p(R|X)}_{\text{posterior}}
      \propto
      \underbrace{p(R)}_{\text{prior}}
      \underbrace{p(X|R)}_{\text{likelihood}}
  \end{equation*}
  \begin{itemize}
    \item Not expecting to identify \emph{true} regulatory network
    \item Focus on \emph{plausible or typical} networks
    \item Propagate uncertainty to downstream analysis
    \item Realistic network \emph{priors} difficult to construct\footcite{VeitchClassRandomGraphs2015}
    \item Principled Bayesian inference methods available
    \item Inference difficult in huge space of networks
  \end{itemize}
  \begin{equation*}
    p(R|X) = \frac{p(R) p(X|R)}{\sum_{R' \in \mathcal{R}} p(R') p(X|R')}
  \end{equation*}
\end{frame}


\begin{frame}{Data modalities}
  \note{
    TODO: Add column with data figure
    \begin{itemize}
      \item Conditions could be knockdown, knockout, overexpression, genetic perturbations
      \item Steady-state less informative than time series, c.f. causality
    \end{itemize}
  }
  \begin{itemize}
    \item High $G$ (RNA-seq) vs. low $G$ (qPCR)
    \item High $C$ (Drop-seq) vs. low $C$
    \item Single condition vs. multiple
      \begin{itemize}
        \item Wild-type vs. perturbation
      \end{itemize}
    \item Steady-state vs. time series
    \item Prior/auxiliary information?
      \begin{itemize}
        \item<2-> Promoter sequences
        \item<2-> TF binding information
        \item<2-> Protein localisation
      \end{itemize}
    \item All combinations of above
  \end{itemize}
\end{frame}


\begin{frame}{Model/inference modalities}
  \note{
    \begin{itemize}
      \item Edge types relate to Lorenz's Markov networks and Bayesian networks
      \item Predictive ability as part of a larger model, or predictive ability under perturbation, c.f. drug discovery
      \item Interpret parameters to understand biology
      \item Collaborators often want gene lists
    \end{itemize}
  }
  \begin{itemize}
    \item Edge type
    \begin{itemize}
      \item directed
      \item undirected
    \end{itemize}
    \item Parameterised edges?
    \begin{itemize}
      \item structure learning
      \item mechanistic edge parameters, e.g. ODE model
    \end{itemize}
    \item Output
    \begin{itemize}
      \item single network
      \item ranked edges
      \item distribution over networks
    \end{itemize}
    \item Purpose
    \begin{itemize}
      \item predictive ability
      \item interpretable parameters
      \item direct further experiments
    \end{itemize}
  \end{itemize}
\end{frame}



\section{Edge-centric methods}

\begin{frame}{Edge-centric methods}
  \note{
    TODO: Talk about fan-out, cascade and fan-in
    \begin{itemize}
      \item Score could be correlation or mutual information
    \end{itemize}
  }
  \begin{columns}[c]
    \begin{column}{.5\textwidth}
      \centering
      \begin{tikzpicture}
        \graph[simple] {
          subgraph K_n [n=8, clockwise, radius=.7in, edges={draw opacity=0.2}];
          1 --[draw opacity=1, thick] 6;
        };
      \end{tikzpicture}
      \begin{itemize}
        \item Score each edge, $S(e): E \to \mathbb{R}$
        \item Rank edges by score
        \item Optionally apply threshold
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \centering
      \begin{tikzpicture}
        \graph[simple] {
          subgraph K_n [n=8, clockwise, radius=.7in, edges={draw opacity=0}];
          1 --[draw opacity=1, thick] 6;
          1 --[draw opacity=1, thick] 2;
          1 --[draw opacity=1, thick] 4;
          8 --[draw opacity=1, thick] 4;
          8 --[draw opacity=1, thick] 7;
          4 --[draw opacity=1, thick] 5;
        };
      \end{tikzpicture} \\
      1 -- 6 \\
      1 -- 2 \\
      1 -- 4 \\
      8 -- 4 \\
      8 -- 7 \\
      4 -- 5 \\
      \dots
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Edge-centric methods}
  \begin{columns}[c]
    \begin{column}{.5\textwidth}
      Pros
      \begin{itemize}
        \item Statistically simpler
        \item Fast to calculate
      \end{itemize}
      Cons
      \begin{itemize}
        \item Interactions harder to spot
        \item Indirect effects can be mistaken for direct effects
      \end{itemize}
    \end{column}
    \begin{column}{.5\textwidth}
      \begin{center}
        Fan-in
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            {X, Y} -> Z
          };
        \end{tikzpicture}
      \end{center}
      \begin{center}
        Cascade
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            X -> Y -> Z
          };
        \end{tikzpicture}
      \end{center}
      \begin{center}
        Fan-out
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            X -> {Y, Z}
          };
        \end{tikzpicture}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Evaluating ranked edges}
  \note{
    Contingency table
  }
  \begin{columns}[c]
    \begin{column}{.75\textwidth}
      \vspace{.5em} \\
      Assume true network structure is known
      \begin{equation*}
        E_\textnormal{true} \subseteq E_\textnormal{all} = \{\{G_i, G_j\}: G_i, G_j \in V\}
      \end{equation*}
      Threshold ranked list of edges at score, $T$
      \begin{align*}
        P &= \{e: S(e) \ge T\} \\
        N &= \{e: S(e) < T\} = E_\textnormal{all} \setminus P
      \end{align*}
      Define true and false positives and negatives
      \begin{align*}
        TP &= |P \cap E_\textnormal{true}| \\
        FN &= |N \cap E_\textnormal{true}| \\
        FP &= |P \setminus E_\textnormal{true}| = |P| - TP \\
        TN &= |N \setminus E_\textnormal{true}| = |N| - FN
        % |E_\textnormal{all}| &= TP + FN + TN + FN
      \end{align*}
    \end{column}
    \begin{column}{.25\textwidth}
      \centering
      \begin{tikzpicture}
        \graph[simple] {
          subgraph K_n [n=8, clockwise, radius=.4in, edges={draw opacity=0}];
          1 --[draw opacity=1, thick] 6;
          1 --[draw opacity=1, thick] 2;
          1 --[draw opacity=1, thick, dashed] 4;
          8 --[draw opacity=1, thick, dashed] 4;
          8 --[draw opacity=1, thick] 7;
          4 --[draw opacity=1, thick, dashed] 5;
        };
      \end{tikzpicture} \\
      1 -- 6 \cmark \\
      1 -- 2 \cmark \\
      1 -- 4 \xmark \\
      8 -- 4 \xmark \\
      8 -- 7 \cmark \\
      4 -- 5 \xmark \\
      \dots
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Edge prediction statistics}
  \begin{align*}
    \textnormal{accuracy} &= \frac{TP + TN}{TP + FN + TN + FN} \\
    \textnormal{precision} &= \frac{TP}{TP + FP} \\
    \textnormal{recall} = \textnormal{true positive rate} &= \frac{TP}{TP + FN} \\
    \textnormal{false positive rate} &= \frac{FP}{TN + FP} \\
  \end{align*}
  What is problem with accuracy? Hint: typically $|E_\textnormal{all}| \gg |E_\textnormal{true}|$ \\
  Standard plots:
  \begin{itemize}
    \item TPR vs. FPR
    \item precision vs. recall
  \end{itemize}
\end{frame}


\begin{frame}{Receiver operating characteristics and precision-recall curves}
  \note{TODO: Mention AUROC and AUPRC}
  \includegraphics[height=.9\textheight]{Figures/DREAM-roc-pr-curves}
  \blfootnote{\cite{MarbachWisdomcrowdsrobust2012}}
  \vspace{.5em}
\end{frame}


\begin{frame}{Correlation measures}
  \emph{Pearson's product moment correlation coefficient}:
  \begin{equation*}
    \rho_{X,Y}
      = \textnormal{corr}(X,Y)
      = \frac{\textnormal{cov}(X, Y)}{\sigma_X \sigma_Y}
      = \frac{\mathbb{E}[(X - \mu_X)(Y - \mu_Y)]}{\sigma_X \sigma_Y}
  \end{equation*}
  where $\mu_X, \mu_Y$ are the means and $\sigma_X, \sigma_Y$ are the standard deviations of $X, Y$
  \begin{itemize}
    \item $\rho_{X,Y} \in [-1,1]$
    \item measures linear relationships
  \end{itemize}
  \emph{Spearman's rank correlation coefficient} is simply Pearson's correlation
  coefficient applied to the ranks of the $X$ and the $Y$.

  \emph{Kendall's tau} is a rank correlation coefficient (the
  fraction of concordant pairs).
\end{frame}


\begin{frame}{Correlation coefficient examples}
  \includegraphics[width=.5\textwidth]{Figures/correlation-unc}
  \includegraphics[width=.5\textwidth]{Figures/correlation-lin} \\
  \includegraphics[width=.5\textwidth]{Figures/correlation-sig}
  \includegraphics[width=.5\textwidth]{Figures/correlation-exp}
\end{frame}


\begin{frame}{DataSaurus}
  \centering
  \includegraphics[height=.7\textheight]{Data/DataSaurus/1datasaurus}
  \blfootnote{\fontsize{5}{6}\selectfont \url{http://www.thefunctionalart.com/2016/08/download-datasaurus-never-trust-summary.html}}
  % \footnote[frame]{\url{http://www.thefunctionalart.com/2016/08/download-datasaurus-never-trust-summary.html}}
\end{frame}


\begin{frame}{DataSaurus dozen}
  \centering
  \includegraphics[height=.8\textheight]{Data/DataSaurus/images/AllDinos}
  % \blfootnote{\tiny{\url{http://www.thefunctionalart.com/2016/08/download-datasaurus-never-trust-summary.html}}}
  % \footnote[frame]{\url{http://www.thefunctionalart.com/2016/08/download-datasaurus-never-trust-summary.html}}
\end{frame}


\begin{frame}{Anscombe's quartet}
  \centering
  \includegraphics[height=.7\textheight]{Figures/anscombes-quartet} \\
  Shared mean, variance, linear fit and correlation.
  See~\cite{SongComparisoncoexpressionmeasures2012} for
  discussion of relevance to gene expression data.
\end{frame}


\begin{frame}{Mutual information (MI)}
  \begin{definition}
    \begin{equation*}
      I(X, Y) = \mathbb{E}_{p(x, y)}\bigg[\log \frac{p(x, y)}{p(x) p(y)}\bigg]
    \end{equation*}
  \end{definition}
  a measure of dependence between two random variables, $X, Y$
  \begin{itemize}
    \item Symmetric
    \item Values in $[0, \infty)$
    \item 0 if and only if $p(x, y) = p(x) p(y)$
    \item Invariant under invertible transformations of $X$ and $Y$
    \item Equivalent to $\textnormal{KL}[p(x, y)||p(x)p(y)]$
  \end{itemize}
  Units are called bits, nats or bans (aka hartleys) for base-2, base-$e$ and base-10 logarithms respectively
\end{frame}


\begin{frame}{Mutual information bivariate normal}
  \begin{align*}
    \begin{bmatrix} X_1 \\ X_2 \end{bmatrix}
      &\sim
      \mathcal{N}\bigg(
        \begin{bmatrix} \mu_1 \\ \mu_2 \end{bmatrix},
        \Sigma
      \bigg)
      , \qquad
      \Sigma = \begin{bmatrix} \sigma_1^2 & \rho \sigma_1 \sigma_2 \\ \rho \sigma_1 \sigma_2 & \sigma_2^2 \end{bmatrix}
  \end{align*}
  \centering
  \includegraphics[height=.5\textheight]{Figures/mi-bivariate}
  \begin{equation*}
    \rho = .7, \qquad I(X_1, X_2) = -\frac{1}{2} \log_2 (1-\rho^2) = .49\textnormal{ bits}
  \end{equation*}
\end{frame}


\begin{frame}{Entropy}
  For a random variable $X$ taking values in a set $\mathcal{X}$, the entropy is
  defined as
  \begin{equation*}
    \mathbb{H}(X) = - \mathbb{E}_{p(x)}[\log p(x)]
  \end{equation*}
  Discrete case:
  \begin{equation*}
    \mathbb{H}(X) = - \sum_{x \in \mathcal{X}} \log p(x) p(x)
  \end{equation*}
  Continuous case:
  \begin{equation*}
    \mathbb{H}(X) = - \int_\mathcal{X} \log p(x) p(x) \diff x
  \end{equation*}
  Relationship to mutual information
  \begin{equation*}
    I(X, Y) = \mathbb{H}(X) + \mathbb{H}(Y) - \mathbb{H}(X,Y)
  \end{equation*}
\end{frame}


\begin{frame}{Conditional entropy}
  \note{
    TODO: Add notes to make sure something interesting to say on this slide
  }
  Conditional entropy:
  \begin{eqnarray*}
    \mathbb{H}(X|Y=y) &=& - \mathbb{E}_{p(x|y)}[p(x|y) \log p(x|y)] \\
    \mathbb{H}(X|Y)   &=& - \mathbb{E}_{p(y)}[\mathbb{H}(X|Y=y)]
  \end{eqnarray*}
  Relationship to mutual information
  \begin{eqnarray*}
    I(X, Y) &=& \mathbb{H}(X) + \mathbb{H}(Y) - \mathbb{H}(X,Y) \\
            &=& \mathbb{H}(X) - \mathbb{H}(X|Y) \\
            &=& \mathbb{H}(Y) - \mathbb{H}(Y|X) % \\
            % &=& \mathbb{H}(X) + \mathbb{H}(Y) - \mathbb{H}(X,Y)
  \end{eqnarray*}
\end{frame}


\begin{frame}{Mutual information estimation}
  \note{
    TODO: Resize figures to match scatter plot/2D histogram sizes
  }
  \begin{itemize}
    \item Correlation is a function of the \emph{data}
    \item MI is a function of the \emph{unknown underlying distribution}
    \item Estimation of MI requires estimation of the distribution
      \begin{itemize}
        \item Often done by 1D and 2D histograms
      \end{itemize}
  \end{itemize}
  \begin{equation*}
    I(X_1, X_2) = \mathbb{H}(X_1) + \mathbb{H}(X_2) - \mathbb{H}(X_1,X_2)
  \end{equation*}
  \includegraphics[width=.5\textwidth]{Figures/mi-bivariate}
  \includegraphics[width=.5\textwidth]{Figures/mi-hist2d}
\end{frame}


\begin{frame}{MI estimators\footcite{Ish-HorowiczMutualInformationEstimation2017}}
  \note{
    \begin{itemize}
      \item Many born out of neuroscience literature for spike train analysis
    \end{itemize}
  }
  \begin{itemize}
    \begin{columns}[c]
      \begin{column}{.5\textwidth}
        \item Histogram maximum likelihood (aka naive, empirical or plug-in)
          \begin{itemize}
            \item Bin locations
              \begin{itemize}
                \item equal width
                \item equal frequency
                \item Bayesian blocks
              \end{itemize}
            \item Number of bins
          \end{itemize}
        \item Kernel density
        \item k-nearest-neighbours
        \item Others: B-spline, Shrinkage, Miller-Madow, Chao-Shen
      \end{column}
      \begin{column}{.5\textwidth}
        \includegraphics[width=.95\textwidth]{Figures/MI-estimators}
        \footnotemark
      \end{column}
      \footcitetext{McMahonInformationtheorysignal2014}
    \end{columns}
  \end{itemize}
  % For a comparison see~\cite{Ish-HorowiczMutualInformationEstimation2017}
\end{frame}


\begin{frame}{Chow-Liu trees}
  \begin{columns}[c]
    \begin{column}{.5\textwidth}
      \centering
      Joint distribution $p(X_1, \dots, X_8)$

      Chow-Liu tree
      \begin{tikzpicture}
        \graph [simple, layered layout] {
          x1[as=$X_1$] -> x2[as=$X_2$] -> x3[as=$X_3$] -> x4[as=$X_4$],
          x3 -> x5[as=$X_5$],
          x1 -> x6[as=$X_6$] -> {x7[as=$X_7$], x8[as=$X_8$]}
        };
      \end{tikzpicture}

      Approximate distribution $p(X_1)p(X_2|X_1)p(X_3|X_2)\dots$
    \end{column}
    \begin{column}{.5\textwidth}
      Chow-Liu algorithm\footnotemark
      \begin{itemize}
        \item Estimate all pair-wise mutual informations, $I(X_i, X_j)$
        \item Calculate maximal spanning tree
      \end{itemize}
      Provides closest possible first-order approximation to true joint distribution
      (in KL-divergence).
    \end{column}
    \footcitetext{ChowApproximatingdiscreteprobability1968}:
  \end{columns}
\end{frame}


\begin{frame}{MI significance}
  \note{
    TODO: Add notes
  }
  Permute genes for each sample/cell \\
  \vspace{1em}
  \begin{columns}[c]
    \begin{column}{.5\textwidth}
      $C \times G$ expression matrix
      \includegraphics[width=.95\textwidth]{Figures/mESC-expr}
    \end{column}
    \begin{column}{.5\textwidth}
      \includegraphics[width=.95\textwidth]{Figures/MI-permutation}
      \footnotemark
    \end{column}
    \footcitetext{PethelExactTestIndependence2014}:
  \end{columns}
\end{frame}


\begin{frame}{Data Processing Inequality}
  \begin{theorem}[DPI\footcite{CoverElementsinformationtheory2006}]
  If $X \to Y \to Z$ is a Markov chain then $I(X, Y) \ge I(X, Z)$
  \end{theorem}
  \vspace{10pt}
  $X, Y, Z$ could be genes but equally applicable to
  \begin{equation*}
    \textnormal{protein} \to \textnormal{mRNA} \to \textnormal{reads} \to
    \textnormal{counts} \to \log \textnormal{counts} + 1
  \end{equation*}
  Summarising data never increases its information content
\end{frame}


\begin{frame}{ARACNE\footcite{MargolinARACNEAlgorithmReconstruction2006}}
  \begin{columns}[c]
    \begin{column}{.7\textwidth}
      \begin{itemize}
        \item Estimate MI significance level, $I_0$
        \item Threshold $I(G_i, G_j) \ge I_0$
        \item Assumption: no 3-cycles
        \item Data processing inequality:
          \begin{equation*}
            I(G_i,G_k) \le \textrm{min}\{I(G_i,G_j), I(G_j,G_k)\}
          \end{equation*}
        \item Generalisation of Chow-Liu trees
        \end{itemize}
    \end{column}
    \begin{column}{.3\textwidth}
      \centering
      \begin{tikzpicture}
        \graph {
          "$G_i$" -- "$G_j$" -- "$G_k$";
        };
      \end{tikzpicture}

      \vspace{1em}
      % \vfill

      \begin{tikzpicture}
        \graph {
          subgraph K_n [n=8, clockwise, radius=.4\textwidth, edges={draw opacity=.5}];
          1 --[draw opacity=1, thick] 2;
          1 --[draw opacity=1, thick] 6;
          2 --[draw opacity=1, thick] 6;
        };
      \end{tikzpicture}
      % \includegraphics[height=.3\textheight]{Figures/DPI}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{Context likelihood of relatedness (CLR)\footcite{FaithLargeScaleMappingValidation2007}}
  \centering
  \includegraphics[width=\textwidth]{Figures/CLR}
  \begin{itemize}
    \item $z_i, z_j$ account for the background distribution of MI scores for gene $i$ and gene $j$
    \item Adjusts for weak correlation between 1 regulator and many target genes (or \emph{vice versa})
    \item 3-way extensions\footcite{WatkinsonInferenceRegulatoryGene2009, ChanGeneRegulatoryNetwork2017}
  \end{itemize}
\end{frame}


\begin{frame}{$\eta^2$-approach: two-way ANOVA\footcite{KuffnerInferringgeneregulatory2012}}
  \note{
    TODO: Check method
  }
  \begin{columns}[c]
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Figures/eta2-approach}
    \end{column}
    \begin{column}{.6\textwidth}
      \begin{eqnarray*}
        Y_{ijk} &=& \mu + \tau_i + \beta_j + \gamma_{ij} + \eta_{ijk} \\
        \textnormal{SS}_T &=& \textnormal{SS}_C + \textnormal{SS}_G + \textnormal{SS}_{CG} + \textnormal{SS}_\textnormal{err} \\
        \eta^2_+ &=& \frac{\textnormal{SS}_C}{\textnormal{SS}_T}
      \end{eqnarray*}
      \centering
      \includegraphics[width=.75\textwidth]{Figures/eta2-results}
    \end{column}
  \end{columns}
  Adjust for negative correlation: $\eta^2 = \max \{\eta^2_-, \eta^2_+\}$
\end{frame}


\section{Gene-centric methods}

\begin{frame}{Gene-centric methods}
  \note{
    TODO: Double check pros and cons against fan-in, cascade and fan-out
  }
  \begin{columns}[c]
    \begin{column}{.65\textwidth}
      \begin{tikzpicture}
        \graph[simple] {
          subgraph K_n [n=8, clockwise, radius=.2\textwidth, edges={draw opacity=0.2}];
          {1, 2, 3, 4, 5, 7, 8} ->[draw opacity=1, thick] 6;
        };
      \end{tikzpicture}
      \begin{itemize}
        \item Regress each node $v \in V$ on other nodes $V \setminus \{v\}$
        \item Rank edges by importance in regression
        \item<2-> Considers several edges
        \item<2-> Some interactions harder to spot
        \item<2-> Indirect effects
        \item<2-> Can be slower than edge-centric
      \end{itemize}
    \end{column}
    \begin{column}{.35\textwidth}
      \begin{center}
        Fan-in
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            {X, Y} -> Z
          };
        \end{tikzpicture}
      \end{center}
      \begin{center}
        Cascade
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            X -> Y -> Z
          };
        \end{tikzpicture}
      \end{center}
      \begin{center}
        Fan-out
        \begin{tikzpicture}[rounded corners]
          \graph [layered layout] {
            X -> {Y, Z}
          };
        \end{tikzpicture}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}


\begin{frame}{MRNET\footcite{MeyerInformationTheoreticInferenceLarge2007}}
  Maximum Relevance/Minimum Redundancy (MRMR) algorithm: for a target gene $Y$,
  initialise $S = \emptyset$ then repeatedly select:
  \begin{equation*}
    X_j^\textnormal{MRMR} = \argmax_{X_j \in V \setminus S}[I(X_j, Y) - \sum_{X_k \in S} I(X_k, X_j)]
  \end{equation*}
  \centering
  \begin{tikzpicture}
    \graph[simple] {
      subgraph K_n [n=8, clockwise, radius=.2\textwidth, edges={draw opacity=0.2}];
      {1, 4, 8} ->[draw opacity=1, thick] 6;
      {2} ->[draw opacity=1, thick, dashed] 6;
      {1, 4, 8} --[draw opacity=1, thick, dotted] 2;
      % {1, 4, 8} ->[draw opacity=1, thick, dotted] 3;
      % {1, 4, 8} ->[draw opacity=1, thick, dotted] 5;
      % {1, 4, 8} ->[draw opacity=1, thick, dotted] 7;
    };
  \end{tikzpicture}
\end{frame}


\begin{frame}{The bootstrap}
  \input{bootstrap-resampling.tex}
  \blfootnote{\fontsize{5}{6}\selectfont Germain Salvato-Vallverdu: \url{http://www.texample.net/tikz/examples/bootstrap-resampling/}}
\end{frame}


\begin{frame}{TIGRESS}\footcite{HauryTIGRESSTrustfulInference2012}
  \centering
  \includegraphics[width=\textwidth]{Figures/TIGRESS-performance}
  \begin{itemize}
    \item least-angle regression (LARS) with stability selection
  \end{itemize}
\end{frame}


\begin{frame}{Random forests}
  \centering
  \includegraphics[width=\textwidth]{Figures/xgboost}
  \blfootnote{XGBoost:~\cite{ChenXGBoostScalableTree2016}}
\end{frame}


\begin{frame}{GENIE3\footcite{Huynh-ThuInferringRegulatoryNetworks2010}}
  \centering
  \includegraphics[width=\textwidth]{Figures/GENIE3}
  \begin{itemize}
    \item Normalise gene expression to have unit variance
    \item Fit a random forest for each gene on all others in turn
    \item Rank features based on reduction in variance
    \item Aggregate ranked features
  \end{itemize}
\end{frame}


\begin{frame}{Jump3\footcite{Huynh-ThuCombiningtreebaseddynamical2015}}
  \note{
    TODO: Check method
  }
  \begin{columns}[c]
    \begin{column}{.3\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Figures/Jump3-GRN}
    \end{column}
    \begin{column}{.7\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Figures/Jump3}
    \end{column}
  \end{columns}
  \begin{equation*}
    \diff x_i = (A_i \mu_i(t) + b_i - \lambda_i x_i) \diff t + \sigma \diff \omega(t)
  \end{equation*}
\end{frame}




\section{Module-based methods}

\begin{frame}{Module-based inference}
  \note{
    TODO: Add more content
  }
  \includegraphics[width=\textwidth]{Figures/mESC-expr}
  \begin{itemize}
    \item Reduce dimensionality by clustering genes and/or cells
    \item Biclustering
  \end{itemize}
\end{frame}


\begin{frame}{Scale-free networks}
  \begin{columns}[c]
    \begin{column}{.4\textwidth}
      \centering
      \includegraphics[width=\textwidth]{Figures/scale-free}
    \end{column}
    \begin{column}{.6\textwidth}
      Degree distribution follows a power-law:
      \begin{align*}
        p(k) &\approx& k^{-\gamma} \\
        \log p(k) &\approx& -\gamma \log k
      \end{align*}
      Few highly-connected hubs \\
      Many genes with few connections \\
      Hierarchical organisation \\
      Controversial reputation
    \end{column}
  \end{columns}
  \blfootnote{\fontsize{5}{6}\selectfont Simon Cockell: \url{https://www.flickr.com/photos/sjcockell/8425835703}}
\end{frame}


\begin{frame}{Weighted gene correlation network
  analysis\footcite{Zhanggeneralframeworkweighted2005,
  LangfelderWGCNApackageweighted2008a}}
  \begin{tabular}{cl}
    \begin{tabular}{c}
      \includegraphics[width=.3\textwidth]{Figures/WGCNA-adj-fn}
    \end{tabular}
    & \begin{tabular}{l}
        \parbox{0.52\textwidth}{%  change the parbox width as appropriate
          \begin{itemize}
            \item Map gene-gene correlations via an adjacency function.
            \item Parameters are chosen to make the network scale-free.
          \end{itemize}
        }
    \end{tabular} \\
    \begin{tabular}{c}
      \includegraphics[width=.3\textwidth]{Figures/WGCNA-network}
    \end{tabular}
    & \begin{tabular}{l}
        \parbox{0.52\textwidth}{%  change the parbox width as appropriate
          \begin{itemize}
            \item This creates a weighted network.
            \item A connectivity-based distance is defined between the genes.
            \item Hierarchical clustering is used to identify modules.
          \end{itemize}
        }
    \end{tabular}
  \end{tabular}
\end{frame}


\begin{frame}{Weighted gene correlation network analysis}
  \includegraphics[height=\textheight]{Figures/wgcna}
\end{frame}


\begin{frame}<presentation:0>{Module-based network inference}
  \note{
    TODO: Check method
    TODO: Remove slide?
  }
  \centering
  \includegraphics[height=.75\textheight]{Figures/Module-based} \\
  \footcite{XuLearningmodulenetworks2004}
\end{frame}


\begin{frame}{Genome-wide discovery of transcriptional modules\footcite{SegalGenomewidediscoverytranscriptional2003}}
  \note{
    TODO: Check method
    TODO: Add content
    TODO: Expand into more slides
  }
  \centering
  \includegraphics[width=\textwidth]{Figures/segal-module-discovery} \\
Models: expression, motif, regulation
\end{frame}


\begin{frame}{DREAM network challenge\footcite{MarbachWisdomcrowdsrobust2012}}
  \includegraphics[height=.9\textheight]{Figures/DREAM-comparison}
\end{frame}


% \section{References}
\begin{frame}[allowframebreaks]{References}
  % \renewcommand*{\bibfont}{\scriptsize}
  % \renewcommand*{\bibfont}{\footnotesize}
  \printbibliography
\end{frame}

\end{document}
